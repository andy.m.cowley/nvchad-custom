local autocmd = vim.api.nvim_create_autocmd

local opt = vim.opt

autocmd("FileType", {
   pattern = ".md",
   callback = function()
      -- vim.opt.laststatus = 0
      opt.textwidth = 100
   end,
})
