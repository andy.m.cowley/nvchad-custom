local M = {}

M.treesitter = {
   ensure_installed = {
     "html",
     "css",
     "bash",
     "dockerfile",
     "dot",
     "go",
     "hcl",
     "json",
     "lua",
     "markdown",
     "python",
     "regex",
     "toml",
     "vim",
     "yaml"
   },
}

M.mason = {
  ensure_installed = {
    "html-lsp",
    "gopls",
    "css-lsp",
    "yaml-language-server",
    "puppet-editor-services",
    "jedi-language-server",
    "json-lsp",
    "vim-language-server",
    "dockerfile-language-server",
    "bash-language-server",
    "sqlls",
    "terraform-ls",
    "tflint",
  },
}

M.nvimtree = {
   open_on_tab = true,
   open_on_setup = true,
   open_on_setup_file = true,
   auto_reload_on_write = true,
   git = {
     enable = true,
     ignore = false,
     timeout = 500,
   },
   renderer = {
   highlight_opened_files = "all",
      highlight_git = true,
      icons = {
         show = {
            git = true,
         },
      },
   },
   filters = {
     dotfiles = true
   },

}
return M
