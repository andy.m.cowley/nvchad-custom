-- This is an example chadrc file , its supposed to be placed in /lua/custom/

require "custom.autocmds"
local g = vim.g

vim.opt.colorcolumn = "100"
vim.opt.cursorcolumn = true
vim.opt.cursorline = true
vim.opt.scrolloff = 5
vim.opt.undofile = true
vim.opt.relativenumber = true
vim.opt.textwidth = 100

local M = {}
-- make sure you maintain the structure of `core/default_config.lua` here,
-- example of changing theme:
M.ui = {
   theme = "gruvbox",
}

-- Install plugins
-- local pluginConfs = require "custom.plugins.configs"
local userPlugins = require "custom.plugins" -- path to table
local override = require "custom.override"

M.options = {
   undofile = true,
   relativenumber = true,
}

M.plugins = {
   user = userPlugins,
   status = {
     dashboard = true,
     alpha = true,
     colorizer = true,
     telescope_media = true,
   },
   options = {
     lspconfig = {
       setup_lspconf = "custom.plugins.lspconfig",
     },
   },
   override = {
     ["williamboman/mason.nvim"] = override.mason,
      ["nvim-treesitter/nvim-treesitter"] = override.treesitter,
      ["kyazdani42/nvim-tree.lua"] = override.nvimtree,
   }
}

return M
