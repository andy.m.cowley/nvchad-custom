local null_ls = require "null-ls"
local b = null_ls.builtins

local sources = {

   b.formatting.prettierd.with {
     filetypes = {
       "html",
       "markdown",
       "css",
       "json",
       "yaml",
       "markdown"
     }
   },

   -- Lua
   b.formatting.stylua,
   b.diagnostics.luacheck.with { extra_args = { "--global vim" } },

   -- Shell
   b.formatting.shfmt,
   b.diagnostics.shellcheck.with { diagnostics_format = "#{m} [#{c}]" },

   -- Python
   b.formatting.black,

   -- Go
   b.formatting.gofmt,

   -- Trim Whitespace
   b.formatting.trim_whitespace,

   -- Ansible
   b.diagnostics.ansiblelint,

   -- Docker
   b.diagnostics.hadolint,

   -- Puppet
   b.diagnostics.puppet_lint,
   b.formatting.puppet_lint,

   -- Terraform
   b.formatting.terraform_fmt,

   -- Vale for markdown
   b.diagnostics.vale,
}

local M = {}

M.setup = function()
   null_ls.setup {
      debug = true,
      sources = sources,

      -- format on save
      on_attach = function(client)
         if client.resolved_capabilities.document_formatting then
            vim.cmd "autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_sync()"
         end
      end,
   }
end

return M
