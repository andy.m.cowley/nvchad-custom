local lspconfig = require("lspconfig")
local on_attach = require("plugins.configs.lspconfig").on_attach
local capabilities = require("plugins.configs.lspconfig").capabilities

   -- lspservers with default config
local servers = {
"html-lsp",
"gopls",
"css-lsp",
"yaml-language-server",
"puppet-editor-services",
"jedi-language-server",
"json-lsp",
"vim-language-server",
"dockerfile-language-server",
"bash-language-server",
"sqlls",
"terraform-ls",
"tflint",
  }

for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
     on_attach = on_attach,
     capabilities = capabilities,
     flags = {
        debounce_text_changes = 150,
     },
  }
end

lspconfig.ltex.language = "en-GB"
lspconfig.ltex.setup {
 on_attach = on_attach,
   capabilities = capabilities,
settings = {
ltex = {
  language = "en-GB",
                    disabledRules = {
                      ["en-GB"] = {
                        "OXFORD_SPELLING_NOUNS"
                      },
                    },
                  },
                }
}
